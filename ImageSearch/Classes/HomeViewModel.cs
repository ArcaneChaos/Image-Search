﻿using Classes;
using System.Collections.Generic;

namespace GoogleImage.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<PictureInformation> Images { get; set; }
    }
}