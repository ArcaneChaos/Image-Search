﻿using Classes;
using System.Collections.Generic;
using System.Net;

namespace Classes
{
    public interface IImageRequest
    {
        string SearchTerm { get; set; }
        string Url { get; }

        IEnumerable<PictureInformation> Parse(string xml);

        ICredentials Credentials { get; }
    }
}