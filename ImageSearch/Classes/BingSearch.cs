﻿using Classes;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml.Linq;

namespace Classes
{
    public class BingRequest : IImageRequest
    {
        private const string AppId = "WUDIF";

        public BingRequest()
        {
            Count = 25;
            Offset = 0;
            Filters = WebUtility.UrlEncode("Style:Photo+Aspect:Wide");
        }

        private string searchTerm;

        public string SearchTerm
        {
            get { return searchTerm; }
            set { searchTerm = value; }
        }

        public string Url
        {
            get
            {
                return string.Format("https://api.datamarket.azure.com/Bing/Search/Image?Query=%27{0}%27&ImageFilters=%27{3}%27&$top={1}&$skip={2}&$format=Atom", SearchTerm, Count, Offset, Filters);
            }
        }

        public int Count { get; set; }
        public int Offset { get; set; }
        public string Filters { get; set; }

        public IEnumerable<PictureInformation> Parse(string xml)
        {
            XElement respXml = XElement.Parse(xml);
            XNamespace d = XNamespace.Get("http://schemas.microsoft.com/ado/2007/08/dataservices");
            XNamespace m = XNamespace.Get("http://schemas.microsoft.com/ado/2007/08/dataservices/metadata");

            return (from item in respXml.Descendants(m + "properties")
                    select new PictureInformation
                    {
                        Title = new string(item.Element(d + "Title").Value.Cast<char>().Take(50).ToArray()),
                        Url = item.Element(d + "MediaUrl").Value,
                        ThumbnailUrl = item.Element(d + "Thumbnail").Element(d + "MediaUrl").Value,
                        Source = "Bing"
                    }).ToList();
        }

        public ICredentials Credentials
        {
            get
            {
                return new NetworkCredential(AppId, "G6e9bjtCg9ZphgoQgleSBtsBXvKGPl+LjwR0VdtaNjM");
            }
        }
    }
}