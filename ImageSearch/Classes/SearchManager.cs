﻿using Classes;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Classes
{
    public class SearchManager
    {
        public async Task<IEnumerable<PictureInformation>> SearchImagesAsync(string searchTerm)
        {
            var request = new BingRequest();
            request.SearchTerm = searchTerm;

            var client = new HttpClient(new HttpClientHandler
            {
                Credentials = request.Credentials
            });
            HttpResponseMessage response = await client.GetAsync(request.Url);
            string resp = await response.Content.ReadAsStringAsync();
            IEnumerable<PictureInformation> images = null;
            await Task.Run(() =>
            {
                images = request.Parse(resp);
            });
            return images;
        }
    }
}