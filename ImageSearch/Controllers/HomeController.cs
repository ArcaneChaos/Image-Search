﻿using Classes;
using GoogleImage.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace GoogleImage.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index(string Keywords = null)
        {
            HomeViewModel HomeViewModel = new HomeViewModel();
            HomeViewModel.Images = Enumerable.Empty<PictureInformation>();

            if (Keywords != null)
            {
                HomeViewModel.Images = await DoSearch(Keywords);
            }

            return View(HomeViewModel);
        }

        private async Task<IEnumerable<PictureInformation>> DoSearch(string keywords)
        {
            SearchManager manager = new SearchManager();
            IEnumerable<PictureInformation> result = await manager.SearchImagesAsync(keywords);
            return result;
        }
    }
}